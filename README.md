# MusicEngine

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Breve analisi tecnica

L'app è stata realizzata in Angular 11.2.2, è responsive ed ha seguito una timelime gestita su trello: https://trello.com/b/cz1Dr2tL/music-engine.
La lista di icone è stata importata utilizzando  https://icomoon.io, mentre i font sono stati selezionati dalla libreria di google font.
L'app presenta due viste oltre a quella di errore 404: la Home Page e un listato di risorse.
La Home page si limita a recepire la richiesta dell'utente o attraverso un input di testo o attraverso gli artisti suggeriti,
mappati all'interno di un file json statico, e crea un url parlante, a cui si viene reindirizzati dopo aver comunicato la richiesta, che passa dinamicamente il dato recepito.
La pagina di listato ricava il dato passato dall'url e si occupa di chiamare le api di ricerca attraverso l'`apiService`.
Le risorse ricevute vengono filtrate e mappate tramite il `listService` e vengono visualizzate all'interno di un carosello generato tramite la libreria 'Swiper'.
Ogni risorsa è rappresentata tramite il componente `resource-carousel` il quale tramite il `playService` gestisce anche la riproduzione della risorsa.
Il 'load more' per ogni carosello è lanciato automaticamente quando si raggiunge l'ultimo elemento tramite l'evento `reachEnd`.
Infine l'app è stata deployata utilizzando il servizio `firebase hosting` all'indirizzo: https://music-engine-a4944.firebaseapp.com/
