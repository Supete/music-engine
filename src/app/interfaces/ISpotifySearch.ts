interface ISpotifySearch {
  error?: boolean;
  tracks: ISpotifySearchTrack;
}

interface ISpotifySearchToken {
  access_token: string;
  expires_in: number;
  token_type: string;
  error?: boolean;
}

interface ISpotifySearchTrack {
  href: string;
  items: ISpotifySearchTrackItem[];
  limit: number;
  next: string;
  offset: number;
  previous: string;
  total: number;
}

interface ISpotifySearchTrackItem {
  album: ISpotifySearchTrackItemAlbum;
  artists: ISpotifySearchTrackItemArtist[];
  available_markets: string[];
  disc_number: number;
  duration_ms: number;
  explicit: boolean;
  external_ids: {
    [key: string]: string;
  };
  external_urls: {
    [key: string]: string;
  };
  href: string;
  id: string;
  is_local: boolean;
  name: string;
  popularity: number;
  preview_url: string;
  track_number: number;
  type: string;
  url: string;
}

interface ISpotifySearchTrackItemArtist {
  external_urls: {
    [key: string]: string;
  };
  href: string;
  id: string;
  name: string;
  type: string;
  url: string;
}

interface ISpotifySearchTrackItemAlbum {
  album_type: string;
  artists: ISpotifySearchTrackItemArtist[];
  available_markets: string[];
  external_urls: {
    [key: string]: string;
  };
  href: string;
  id: string;
  images: ISpotifySearchTrackItemAlbumImage[];
  name: string;
  relase_date: string;
  relase_date_precision: string;
  total_tracks: number;
  type: string;
  url: string;
}

interface ISpotifySearchTrackItemAlbumImage {
  height: number;
  width: number;
  url: string;
}

export {
  ISpotifySearch,
  ISpotifySearchToken,
  ISpotifySearchTrack,
  ISpotifySearchTrackItem,
  ISpotifySearchTrackItemArtist,
  ISpotifySearchTrackItemAlbum,
  ISpotifySearchTrackItemAlbumImage
};

