interface IYoutubeSearch {
  etag: string;
  items: IYoutubeSearchItem[];
  kind: string;
  nextPageToken: string;
  pageInfo: any;
  regionCode: string;
  error?: boolean;
}

interface IYoutubeSearchItem {
  etag: string;
  id: any;
  kind: string;
  snippet: any;
}

export {
  IYoutubeSearch,
  IYoutubeSearchItem
};
