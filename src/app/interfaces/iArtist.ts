export interface IArtist {
  name: string;
  url: string;
  imageUrl: string;
}
