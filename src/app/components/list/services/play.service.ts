import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayService {
  isResourcePlaying$: BehaviorSubject<boolean>;

  constructor() {
    this.isResourcePlaying$ = new BehaviorSubject<boolean>(false);
  }
}
