import { Injectable } from '@angular/core';
import { IArtistResources} from '../interfaces/iArtistResources';
import { Resource } from '../classes/Resource';
import { BehaviorSubject } from 'rxjs';
import { ISpotifySearchTrack } from '../../../interfaces/ISpotifySearch';
import { IYoutubeSearch } from '../../../interfaces/IYoutubeSearch';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  artistResources: IArtistResources;
  loadMoreResources$: BehaviorSubject<any>;

  constructor() {
    this.artistResources = {audio: [], video: []};
    this.loadMoreResources$ = new BehaviorSubject(null);
  }

  artistResourcesAdapter(data): IArtistResources {
    this.artistResources.audio = this.spotifyResourcesAdapter(data.spotifyResources.tracks);
    this.artistResources.video = this.youtubeResourcesAdapter(data.youTubeResources);

    return this.artistResources;
  }

  spotifyResourcesAdapter(data: ISpotifySearchTrack): Resource[] {
    const spotifyResources: Resource[] = [];
    data.items.forEach((song) => {
      const tempResource = new Resource(song.name, song.external_urls.spotify, song.album.images, 'audio', song.preview_url);
      spotifyResources.push(tempResource);
    });
    return spotifyResources;
  }

  youtubeResourcesAdapter(data: IYoutubeSearch): Resource[] {
    const youtubeResources: Resource[] = [];
    data.items.forEach((song) => {
      if (song.id.videoId) {
        const gallery = Object.values(song.snippet.thumbnails).sort((a: any, b: any) => {
          return b.width - a.width;
        });
        const tempResource = new Resource(song.snippet.title, song.id.videoId, gallery, 'video');
        youtubeResources.push(tempResource);
      }
    });
    return youtubeResources;
  }

  getMoreResources(swiperSelected): void {
    this.loadMoreResources$.next(swiperSelected);
  }
}
