import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ActivatedRoute} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {switchMap} from 'rxjs/internal/operators';
import {IArtistResources} from './interfaces/iArtistResources';
import {ListService} from './services/list.service';
import {Subscription} from 'rxjs';
import {ChangeDetectorRef} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  artistName$: Observable<any>;
  artistName: string;
  artistResources: IArtistResources;
  loadMoreResourcesSub: Subscription;
  audioResurcesOffset: number;
  videoResurcesOffset: string;
  isAudioResourceLoading: boolean;
  isVideoResourceLoading: boolean;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private listService: ListService,
    private cdr: ChangeDetectorRef,
    private titleService: Title
  ) {
    this.artistName$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.artistName = String(params.get('artistName')).replace(/-/g, ' ');
        this.titleService.setTitle(`Music Engine: ${this.artistName}`);
        return this.doSearch(this.artistName);
      })
    );
  }

  ngOnInit(): void {
    this.isAudioResourceLoading = true;
    this.isVideoResourceLoading = true;
    this.audioResurcesOffset = 0;
    this.artistName$.subscribe(data => {
      this.videoResurcesOffset = data.youTubeResources.nextPageToken;
      this.artistResources = this.listService.artistResourcesAdapter(data);
      this.isAudioResourceLoading = false;
      this.isVideoResourceLoading = false;
      // console.log(this.artistResources);
    });
    this.loadMoreResourcesSub = this.listService.loadMoreResources$
      .subscribe(swiper => {
        if (swiper) {
          if (swiper.type === 'audio') {
            this.isAudioResourceLoading = true;
            this.audioResurcesOffset += 20;
            this.apiService.getSpotifyData(this.artistName, this.audioResurcesOffset.toString())
              .subscribe(newResources => {
                this.artistResources.audio = this.artistResources.audio.concat(this.listService.spotifyResourcesAdapter(newResources.tracks));
                this.isAudioResourceLoading = false;
                this.cdr.detectChanges();
                setTimeout(() => {
                  swiper.slideTo(this.audioResurcesOffset - 1, 0);
                }, 10);
                // console.log(this.artistResources)
              });
          } else {
            this.isVideoResourceLoading = true;
            this.apiService.getYouTubeData(this.artistName, this.videoResurcesOffset)
              .subscribe(newResources => {
                this.videoResurcesOffset = newResources.nextPageToken;
                this.artistResources.video = this.artistResources.video.concat(this.listService.youtubeResourcesAdapter(newResources));
                this.isVideoResourceLoading = false;
                this.cdr.detectChanges();
                setTimeout(() => {
                  swiper.slideTo(this.artistResources.video.length - 9, 0);
                }, 10);
                // console.log(this.artistResources)
              });
          }
        }
      });
  }

  private doSearch(artist: string): Observable<any> {
    return forkJoin({
      youTubeResources: this.apiService.getYouTubeData(artist),
      spotifyResources: this.apiService.getSpotifyData(artist)
    });
  }

  ngOnDestroy(): void {
    this.loadMoreResourcesSub.unsubscribe();
  }
}
