import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Resource } from '../../classes/Resource';
import { ListService } from '../../services/list.service';
import SwiperCore, {Pagination} from 'swiper/core';
SwiperCore.use([Pagination]);
import {ChangeDetectorRef} from '@angular/core';
import { Subscription } from 'rxjs';
import { PlayService } from '../../services/play.service';

@Component({
  selector: 'app-resource-carousel',
  templateUrl: './resource-carousel.component.html',
  styleUrls: ['./resource-carousel.component.scss']
})
export class ResourceCarouselComponent implements OnInit {
  @Input() artistResourcesByType: any[];
  @ViewChild('swiperWrapper') swiperWrapper: any;
  @ViewChild('youtubePlayer') youtubePlayer: any;

  audioItem: any;
  isSomethingPlaying: boolean;
  isSomethingPlayingSub: Subscription;

  constructor(private listService: ListService,  private cdr: ChangeDetectorRef, private playService: PlayService) { }

  ngOnInit(): void {
    this.isSomethingPlaying = false;
    this.isSomethingPlayingSub = this.playService.isResourcePlaying$.subscribe(bool => {
      this.isSomethingPlaying = bool;
    });
  }

  onLastSlide(swiper): void {
    swiper.type = this.artistResourcesByType[0].type;
    this.listService.getMoreResources(swiper);
  }

  playAudio(item: Resource): void {
    if (!this.isSomethingPlaying) {
      this.playService.isResourcePlaying$.next(true);
      item.isPlaying = true;
      this.audioItem = new Audio();
      this.audioItem.src = item.preview;
      this.audioItem.load();
      this.audioItem.play();
      this.cdr.detectChanges();
      setTimeout(() => {
        this.stopAudio(item);
      }, 30000);
    }
  }

  stopAudio(item: Resource): void {
    this.playService.isResourcePlaying$.next(false);
    this.audioItem.pause();
    item.isPlaying = false;
    this.cdr.detectChanges();
  }

  playVideo(item: Resource): void {
    if (!this.isSomethingPlaying) {
      item.isPlaying = true;
      this.playService.isResourcePlaying$.next(true);
      this.cdr.detectChanges();
    }
  }

  checkVideoStatus(event, item): void {
    switch (event.data) {
      case 0:
      case 2:
        item.isPlaying = false;
        this.playService.isResourcePlaying$.next(false);
        break;
      case 1:
        this.playService.isResourcePlaying$.next(true);
        break;
      case -1:
      case 3:
      case 5:
        console.log('you tube state not important');
        break;
      default:
        throw new Error('you tube state not recognized');
    }
  }
}
