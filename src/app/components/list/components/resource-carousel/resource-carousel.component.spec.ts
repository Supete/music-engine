import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceCarouselComponent } from './resource-carousel.component';

describe('ResourceCarouselComponent', () => {
  let component: ResourceCarouselComponent;
  let fixture: ComponentFixture<ResourceCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
