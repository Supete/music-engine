export class Resource {
  name: string;
  href: any;
  gallery: any[];
  type: string;
  isPlaying: boolean;
  preview?: string;

  constructor(name: string, href: any, gallery: any[], type: string, preview: string = null) {
   this.name = name;
   this.href = href;
   this.gallery = gallery;
   this.type = type;
   this.isPlaying = false;
   this.preview = preview;
  }
}
