import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { IArtist } from '../../interfaces/iArtist';
import { Router } from '@angular/router';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  topArtistList: IArtist[];
  artistQuery: string;

  constructor(private apiService: ApiService, private router: Router, private utilsService: UtilsService) { }

  ngOnInit(): void {
    this.apiService.getTopArtistList()
      .then(data => {
        this.topArtistList = data.topArtists;
      })
      .catch(error => {
        console.error(error);
    });
  }

  goToArtistResourceList(): void {
    const artistUrl = this.utilsService.sanitizeForUrl(this.artistQuery);
    if (artistUrl !== '') {
      this.router.navigate(['/artist', artistUrl]);
    }
  }
}
