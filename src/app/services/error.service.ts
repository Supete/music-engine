import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  showFatalError(): void {
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong! Please try again later'
    });
  }
}
