import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { ErrorService } from './error.service';
import { ISpotifySearchToken, ISpotifySearch } from '../interfaces/ISpotifySearch';
import { tap, catchError, retry, shareReplay, switchMap } from 'rxjs/operators';
import { IYoutubeSearch } from '../interfaces/IYoutubeSearch';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  spotifySecretKey = 'ZTAzN2VjNTg2ZDczNDU0NWJiMDQ3NzAxN2MwNDY4YTM6MjEwYzkxMGRmZWMyNGZjMWI0N2JmYzgxNWVmNDI1MjI';
  spotifyTempTokenResponse: ISpotifySearchToken;
  youTubeKey = 'AIzaSyAVnqzbGrViErF03L7uMWOLiezm9LIL_s4';

  constructor(private http: HttpClient, private errorService: ErrorService) { }

  private httpGetHandler(url: string, method: string, httpOptions = {}): Observable<any>{
    return this.http.get<any>(url, httpOptions).pipe(
      retry(3),
      shareReplay({bufferSize: 1, refCount: true}),
      tap(resp => {
        if (resp.error) {
          this.errorService.showFatalError();
          throw new Error(`Error on ${method}`);
        }
      }),
      catchError(e => {
        console.error(e);
        this.errorService.showFatalError();
        return of([]);
      })
    );
  }

  private getSpotifyAccessToken(): Observable<ISpotifySearchToken> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${this.spotifySecretKey}`
      })
    };
    const body = new HttpParams().set('grant_type', 'client_credentials');
    return this.http.post<ISpotifySearchToken>('https://accounts.spotify.com/api/token', body, headers).pipe(
      retry(3),
      shareReplay({bufferSize: 1, refCount: true}),
      tap(resp => {
        if (resp.error) {
          this.errorService.showFatalError();
          throw new Error(`Error on getSpotifyAccessToken`);
        }
      }),
      catchError(e => {
        console.error(e);
        this.errorService.showFatalError();
        return of(null);
      })
    );
  }

  private getSpotifyResources(response: ISpotifySearchToken, query: string, offset: string): Observable<ISpotifySearch> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `${response.token_type} ${response.access_token}`
      }),
      params: new HttpParams().set('q', query).set('type', 'track').set('offset', offset)
    };
    return this.httpGetHandler('https://api.spotify.com/v1/search', 'getSpotifyResources', httpOptions);
  }

  getSpotifyData(artist, offset: string = '0'): Observable<ISpotifySearch> {
    if (this.spotifyTempTokenResponse) {
      return this.getSpotifyResources(this.spotifyTempTokenResponse, artist, offset);
    }
    return this.getSpotifyAccessToken().pipe(
      switchMap(response => {
          this.spotifyTempTokenResponse = response;
          return this.getSpotifyResources(response, artist, offset);
        }
      )
    );
  }

  getYouTubeData(query: string, offset: string = ''): Observable<IYoutubeSearch> {
    const httpOptions = {
      params: new HttpParams().set('part', 'snippet').set('maxResults', '10').set('key', this.youTubeKey).set('q', query).set('pageToken', offset)
    };
    return this.httpGetHandler('https://www.googleapis.com/youtube/v3/search', 'getYouTubeData', httpOptions);
  }

  getTopArtistList(): Promise<any> {
    return this.httpGetHandler('./assets/files/topArtists.json', 'getTopArtistList').toPromise();
  }
}
