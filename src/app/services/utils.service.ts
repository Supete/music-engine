import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  sanitizeForUrl = (input: string): string => {
    return input.normalize('NFD')                       // Change diacritics
      .replace(/[\u0300-\u036f]/g, '') // Remove illegal characters
      .replace(/\s+/g, '-')            // Change whitespaces to dashes
      .replace(/,/g, '-')              // Change commas to dashes
      .replace(/&/g, '-and-')          // Replace ampersand
      .replace(/[^a-z0-9\-]/g, '')     // Remove anything that is not a letter, number or dash
      .replace(/-+/g, '-')             // Remove duplicate dashes
      .replace(/^-*/, '')              // Remove starting dashes
      .replace(/-*$/, '')              // Remove trailing dashes
      .replace(/\./g, '')              // Remove dot
      .toLowerCase();                                         // Change to lowercase
  }
}
